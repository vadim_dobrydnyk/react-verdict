import Api from '../../api/Api';
import auth from '../../auth/authenticator';
import * as types from './actionTypes';

export function loginSuccess() {
  return { type: types.LOG_IN_SUCCESS };
}

export function loginFailure() {
  return { type: types.LOG_IN_FAILURE };
}

export function loginUser(credentials) {
  return function(dispatch) {
    return Api.login(credentials)
      .then(response => {
        // debugger

        if (!response.error) {
          localStorage.setItem("jwt", response.response.jwt);
          dispatch(loginSuccess());
        } else {
          dispatch(loginFailure());
        }
      })
      .catch(error => {
        // debugger;
        dispatch(loginFailure());
      });
  };
}

export function logOutUser() {
  auth.logOut();
  return { type: types.LOG_OUT };
}

export function userExpired() {
  auth.logOut();
  return { type: types.USER_EXPIRED };
}
