import Api from '../../api/Api';
import * as types from './actionTypes';
import { userExpired } from './sessionActions';

export function loadPredictionsSuccess(response) {
  return { type: types.PREDICTION_LOAD_SUCCESS, payload: response.response };
}

export function loadUserPredictionsSuccess(response) {
  return {
    type: types.USER_PREDICTION_LOAD_SUCCESS,
    payload: response.response
  };
}

export function loadUserReportsSuccess(response) {
  return { type: types.USER_REPORTS_LOAD_SUCCESS, payload: response.response };
}

export function loadUserReportsPredictionsSuccess(response) {
  return {
    type: types.USER_REPORTS_PREDICTIONS_LOAD_SUCCESS,
    payload: response.response
  };
}

export function editPredictionSuccess(response, predictionId) {
  return {
    type: types.PREDICTION_EDIT_SUCCESS,
    payload: {
      ...response.response,
      predictionId
    }
  };
}

export function loadPredictions(params, dispatch) {
  return function(dispatch) {
    return Api.predictions(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadPredictionsSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function loadUserPredictions(params, dispatch) {
  return function(dispatch) {
    return Api.predictions(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadUserPredictionsSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function loadUserReports(params, dispatch) {
  return function(dispatch) {
    return Api.reports(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadUserReportsSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function loadUserReportsPredictions(params, dispatch) {
  return function(dispatch) {
    return Api.reports(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadUserReportsPredictionsSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function editPrediction(params, dispatch) {
  return function(dispatch) {
    return Api.editPrediction(params).then(response => {
      return dispatch(editPredictionSuccess(response, params.predictionId));
    });
  };
}

export function loadPredictionsFailure() {
  return { type: types.PREDICTION_LOAD_FAILURE };
}
