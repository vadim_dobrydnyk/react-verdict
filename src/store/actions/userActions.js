import Api from '../../api/Api';
import * as types from './actionTypes';
import { userExpired } from './sessionActions';

export function loadUsersSuccess(response) {
  return { type: types.USERS_LOAD_SUCCESS, payload: response.response };
}

export function loadUserSuccess(response) {
  return { type: types.USER_LOAD_SUCCESS, payload: response.response };
}

export function loadMoreUsersSuccess(response) {
  return { type: types.USERS_MORE_LOAD_SUCCESS, payload: response.response };
}

export function loadMoreUsersPending() {
  return { type: types.USERS_MORE_PENDING };
}

export function loadUsers(params, dispatch) {
  return function(dispatch) {
    return Api.users(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadUsersSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function loadMoreUsers(params, dispatch) {
  return function(dispatch) {
    dispatch(loadMoreUsersPending());

    return Api.users(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadMoreUsersSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function loadUser(params, dispatch) {
  return function(dispatch) {
    return Api.user(params).then(response => {
      return response.hasOwnProperty("response")
        ? dispatch(loadUserSuccess(response))
        : dispatch(userExpired());
    });
  };
}

export function blockUser(userId, value) {
  Api.blockUser(userId, value);
  return { type: types.USER_BLOCK };
}
