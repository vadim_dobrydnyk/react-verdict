import { combineReducers } from 'redux';

import prediction from './predictionReducer';
import session from './sessionReducer';
import users from './userReducer';

export const rootReducer = combineReducers({
  session,
  users,
  prediction
});
