import * as types from '../actions/actionTypes';

const initialState = {
  users: [],
  currentUser: {},
  isUsersLoaded: true
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case types.USER_LOAD_SUCCESS:
      return {
        ...state,
        currentUser: action.payload
      };

    case types.USERS_LOAD_SUCCESS:
      return {
        ...state,
        users: action.payload,
        isUsersLoaded: true
      };

    case types.USERS_MORE_LOAD_SUCCESS:
      return action.payload.length
        ? {
            ...state,
            users: state.users.concat(action.payload),
            isUsersLoaded: true
          }
        : {
            ...state,
            isUsersLoaded: true
          };

    case types.USERS_MORE_PENDING:
      return {
        ...state,
        isUsersLoaded: false
      };

    case types.USER_BLOCK:
      let blocked = state.currentUser.blocked;

      return {
        ...state,
        currentUser: { ...state.currentUser, blocked: !blocked }
      };

    default:
      return state;
  }
}
