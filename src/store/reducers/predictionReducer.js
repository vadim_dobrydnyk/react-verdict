import * as types from '../actions/actionTypes';

const initialState = {
  predictions: [],
  usersPredictions: [],
  usersReports: [],
  usersReportsPredictions: []
};

export default function predictionReducer(state = initialState, action) {
  switch (action.type) {
    case types.USER_PREDICTION_LOAD_SUCCESS:
      return {
        ...state,
        usersPredictions: action.payload
      };

    case types.PREDICTION_LOAD_SUCCESS:
      return {
        ...state,
        predictions: action.payload
      };

    case types.USER_REPORTS_LOAD_SUCCESS:
      return {
        ...state,
        usersReports: action.payload
      };

    case types.USER_REPORTS_PREDICTIONS_LOAD_SUCCESS:
      return {
        ...state,
        usersReportsPredictions: action.payload
      };

    case types.PREDICTION_EDIT_SUCCESS:
      return {
        ...state,
        predictions: state.predictions.map(prediction => {
          return prediction.id === action.payload.predictionId &&
            action.payload.updated
            ? { ...prediction, verdict: prediction.verdict === 1 ? 0 : 1 }
            : prediction;
        })
      };

    default:
      return state;
  }
}
