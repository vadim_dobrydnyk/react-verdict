import * as types from '../actions/actionTypes';

const initialState = {
  session: !!localStorage.jwt,
  error: false
};

export default function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case types.LOG_IN_SUCCESS:
      return {
        ...state,
        error: false,
        session: !!localStorage.jwt
      };

    case types.LOG_OUT:
      return {
        ...state,
        session: !!localStorage.jwt
      };

    case types.LOG_IN_FAILURE:
      return {
        ...state,
        error: true
      };

    case types.USER_EXPIRED:
      return {
        ...state,
        session: false
      };

    default:
      return state;
  }
}
