class Api {
  static host = "http://138.197.178.45:8000";

  static apiFetch(request) {
    return fetch(request)
      .then(response => {
        return response.json();
      })
      .catch(error => {
        return error;
      });
  }

  static paramsString(params) {
    return (
      "?" +
      Object.entries(params)
        .map(([key, value]) => {
          return `${key}=${value}`;
        })
        .join("&")
    );
  }

  static login(credentials) {
    const request = new Request(this.host + `/admin/sing-in`, {
      method: "POST",
      headers: new Headers({ "Content-Type": "application/json" }),
      body: JSON.stringify(credentials)
    });

    return this.apiFetch(request);
  }

  static user(params) {
    const paramsStr = this.paramsString(params);
    const url = this.host + "/admin/user" + paramsStr;
    const request = new Request(url, {
      method: "GET",
      headers: new Headers({ Authorization: localStorage.jwt })
    });

    return this.apiFetch(request);
  }

  static users(params) {
    const paramsStr = this.paramsString(params);
    const url = this.host + "/admin/users" + paramsStr;
    const request = new Request(url, {
      method: "GET",
      headers: new Headers({ Authorization: localStorage.jwt })
    });

    return this.apiFetch(request);
  }

  static predictions(params) {
    const paramsStr = this.paramsString(params);

    const url = this.host + "/admin/predictions" + paramsStr;
    const request = new Request(url, {
      method: "GET",
      headers: new Headers({ Authorization: localStorage.jwt })
    });

    return this.apiFetch(request);
  }

  static blockUser(id, reportValue) {
    const url = this.host + "/block/user";

    const request = new Request(url, {
      method: "PATCH",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: localStorage.jwt
      }),
      body: JSON.stringify({ block_user_id: id })
    });

    return this.apiFetch(request);
  }

  static editPrediction(params) {
    const url = this.host + "/admin/prediction/edit";
    const request = new Request(url, {
      method: "PATCH",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: localStorage.jwt
      }),
      body: JSON.stringify({
        prediction_id: params.predictionId,
        verdict: params.verdict
      })
    });

    return this.apiFetch(request);
  }

  static reports(params) {
    const paramsStr = this.paramsString(params);
    const url = this.host + "/admin/reports" + paramsStr;
    const request = new Request(url, {
      method: "GET",
      headers: new Headers({ Authorization: localStorage.jwt })
    });

    return this.apiFetch(request);
  }
}

export default Api;
