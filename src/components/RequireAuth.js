import React from 'react';
import { connect } from 'react-redux';

export default ComposedComponent => {
  class RequireAuth extends React.Component {
    componentWillMount() {
      if (!this.props.session) {
        this.props.history.push("/login");
      }
    }

    componentWillReceiveProps(nextProps) {
      if (!nextProps.session) {
        this.props.history.push("/login");
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return {
      session: state.session.session
    };
  }

  return connect(mapStateToProps)(RequireAuth);
};
