import '../style/UsersPage.css';

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Header from '../components/Header';
import Modal from '../components/modalWindow';
import * as img from '../images/default-avatar.jpg';
import { loadUserPredictions, loadUserReports, loadUserReportsPredictions } from '../store/actions/predictionActions';
import { blockUser, loadUser } from '../store/actions/userActions';

class UsersPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: [
        { id: 1, name: "Predictions", hide: false, props: "usersPredictions" },
        { id: 2, name: "Reports", hide: true, props: "usersReports" },
        {
          id: 3,
          name: "Predictions reports",
          hide: true,
          props: "usersReportsPredictions"
        }
      ],
      modal: {
        title: "",
        show: false
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.modalHendler = this.modalHendler.bind(this);
    this.tabClick = this.tabClick.bind(this);
  }

  componentWillMount() {
    let id = +this.props.match.params.id;

    this.props.loadUser({ user_id: id });
    this.props.loadUserPredictions({ user_id: id });
    this.props.loadUserReports({
      user_id: id,
      type: 1
    });
    this.props.loadUserReportsPredictions({
      user_id: id,
      type: 2
    });
  }

  modalHendler(answer) {
    if (answer) {
      this.props.blockUser(
        this.props.currentUser.id,
        !this.props.currentUser.blocked
      );
    }
    this.setState({
      modal: { title: "", show: false }
    });
  }

  handleChange(event) {
    const title = this.props.currentUser.blocked
      ? "You want unblock current user?"
      : "You want block current user?";
    this.setState({
      modal: { ...this.state.modal, show: true, title }
    });
  }

  tabClick(event) {
    const id = +event.target.getAttribute("data-id");
    let tabs = this.state.tabs.map(tab => {
      tab.hide = !(tab.id === id);
      return tab;
    });
    this.setState({ tabs });
  }

  render() {
    let user = this.props.currentUser;

    let userInfo = user ? (
      <div className="user-info">
        <img alt="user avatar" src={user.avatar ? user.avatar : img} />
        <p>
          User id <span>{user.id}</span>
        </p>
        <p>
          User name <span>{user.username}</span>
        </p>
        <p>
          First name <span>{user.first_name}</span>
        </p>
        <p>
          Last name <span>{user.last_name}</span>
        </p>
        <p>
          Email <span>{user.email}</span>
        </p>
        <p>
          User is {!!this.props.currentUser.blocked ? "blocked" : "unblocked"}
          <button
            onClick={this.handleChange}
            className={`${!!this.props.currentUser.blocked
              ? "btn-success"
              : "btn-danger"} btn`}
          >
            {!!this.props.currentUser.blocked ? "Unblock" : "Block"}
          </button>
        </p>
      </div>
    ) : (
      ""
    );

    let tabsTitles = this.state.tabs.map((tab, tabIndex) => (
      <h4
        data-id={tab.id}
        key={tab.id}
        onClick={this.tabClick}
        className={tab.hide ? "" : "active-tab"}
      >
        {tab.name}
      </h4>
    ));

    let propsLoaded =
      this.props.usersPredictions &&
      this.props.usersReports &&
      this.props.usersReportsPredictions;

    let tabs =
      propsLoaded &&
      this.state.tabs.map((tab, tabIndex) => {
        if (!this.props[tab.props]) return null;

        let tabItems = this.props[tab.props].map((item, itemIndex) => {
          const dueData = item.due_date ? <p>{item.due_date}</p> : null;
          return (
            <div className="userspage-list-item" key={itemIndex}>
              <p className="text">
                {item.description ? item.description : "No message"}
              </p>
              {dueData}
              <p />
            </div>
          );
        });

        return (
          <div
            className={tab.hide ? "hide-block" : "show-block"}
            key={tabIndex}
          >
            {tabItems}
          </div>
        );
      });

    return (
      <div>
        <Header session={this.props.session} />
        <Modal
          onClick={this.modalHendler}
          show={this.state.modal.show}
          title={this.state.modal.title}
        />
        <div className="container">
          <div className="top-bar">
            <Link to="/users">
              <i className="icon icon-left-circled" /> Back to users list
            </Link>
          </div>
          <div className="main">
            {userInfo}

            <div className="tabs">
              <div className="tabs-titles">{tabsTitles}</div>
              {tabs}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session.session,
    currentUser: state.users.currentUser,
    usersPredictions: state.prediction.usersPredictions,
    usersReports: state.prediction.usersReports,
    usersReportsPredictions: state.prediction.usersReportsPredictions
  };
}

export default connect(mapStateToProps, {
  loadUser,
  blockUser,
  loadUserPredictions,
  loadUserReports,
  loadUserReportsPredictions
})(UsersPage);
