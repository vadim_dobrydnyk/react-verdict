import '../style/Login-page.css';

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as sessionActions from '../store/actions/sessionActions';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      credentials: {
        email: "",
        password: ""
      }
    };
    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentWillMount() {
    if (this.props.session) {
      this.props.history.push("/users");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.session) {
      this.props.history.push("/users");
    }
  }

  onChange(event) {
    const field = event.target.name;
    const credentials = this.state.credentials;
    credentials[field] = event.target.value;
    return this.setState({ credentials: credentials });
  }

  onSave(event) {
    event.preventDefault();
    this.props.actions.loginUser(this.state.credentials);
  }

  render() {
    return (
      <div>
        <form className="form">
          <h2>Verdict admin panel</h2>
          <input
            name="email"
            label="email"
            placeholder="Email"
            value={this.state.credentials.email}
            onChange={this.onChange}
          />
          <input
            name="password"
            label="password"
            type="password"
            placeholder="Password"
            value={this.state.credentials.password}
            onChange={this.onChange}
          />
          <p className={this.props.error ? "show-error" : "hide-error"}>
            Email or password is incorrect
          </p>
          <input
            type="submit"
            className="btn btn-primary"
            onClick={this.onSave}
          />{" "}
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session.session,
    error: state.session.error
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
