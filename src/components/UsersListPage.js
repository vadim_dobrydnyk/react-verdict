import '../style/UsersListPage.css';

import React from 'react';
import { DebounceInput } from 'react-debounce-input';
import { connect } from 'react-redux';

import Header from '../components/Header';
import Color from '../helpers/Color';
import { loadMoreUsers, loadUsers } from '../store/actions/userActions';

class UsersListPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      page: 1,
      limit: 20
    };
    this.onSearch = this.onSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.linkToUser = this.linkToUser;
  }

  handleScroll(event) {
    if (
      window.innerHeight + window.pageYOffset + 200 >=
        window.document.body.clientHeight &&
      this.props.isUsersLoaded
    ) {
      this.setState({ page: ++this.state.page });
      this.props.loadMoreUsers({
        query: this.state.search,
        limit: this.state.limit,
        page: this.state.page
      });
    }
  }

  onSearch(event) {
    const search = event.target.value;
    this.props.loadUsers({ query: search, page: 1, limit: this.state.limit });
    this.setState({ search, page: 1 });
  }

  linkToUser(userId) {
    this.props.history.push(`/users/${userId}`);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);

    this.props.session &&
      this.props.loadUsers({
        query: this.state.search,
        limit: this.state.limit,
        page: this.state.page
      });
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  render() {
    let users = this.props.users.map((user, i) => {
      const color = Color.getRandomColor();
      const letter = user.first_name.charAt(0);
      return (
        <div
          onClick={this.linkToUser.bind(this, user.id)}
          className="users-list-item"
          key={i}
        >
          <img
            letter={letter}
            style={{ backgroundColor: color }}
            className="avatar"
            alt=""
            src={user.small_avatar ? user.small_avatar : ""}
          />
          <div className="stack-list">
            <p>
              {user.first_name} {user.last_name}
            </p>
            <p>
              Token: <code>{user.tokens}</code>
            </p>
            <p>Hit rate: {`${user.hit_rate}%`}</p>
          </div>
        </div>
      );
    });

    return (
      <div>
        <Header session={this.props.session} />
        <div className="container">
          <DebounceInput
            minLength={2}
            debounceTimeout={300}
            className="search"
            placeholder="Enter users name..."
            onChange={this.onSearch}
            value={this.state.search}
            type="text"
          />
          {users}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session.session,
    users: state.users.users,
    isUsersLoaded: state.users.isUsersLoaded
  };
}

export default connect(mapStateToProps, { loadUsers, loadMoreUsers })(
  UsersListPage
);
