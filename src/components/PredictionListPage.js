import '../style/PreditionListPage.css';

import React from 'react';
import { DebounceInput } from 'react-debounce-input';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Header from '../components/Header';
import { editPrediction, loadPredictions } from '../store/actions/predictionActions';

class PredictionListPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""
    };
    this.onSearch = this.onSearch.bind(this);
  }

  onSearch(event) {
    const search = event.target.value;
    this.props.loadPredictions({ query: search });
    this.setState({ search });
  }

  changePrediction(predictionId, verdict) {
    this.props.editPrediction({
      predictionId,
      verdict: verdict ? "1" : "0"
    });
  }

  componentDidMount() {
    this.props.session &&
      this.props.loadPredictions({ query: this.state.search });
  }

  render() {
    let predictions = this.props.predictions.map((prediction, i) => {
      let verdict =
        prediction.verdict in [0, 1] ? (
          <p>
            {"Verdict: "}
            {prediction.verdict ? "has come true " : "hasn't come true "}
            <button
              onClick={this.changePrediction.bind(
                this,
                prediction.id,
                !prediction.verdict
              )}
              className={`${prediction.verdict
                ? "btn-danger"
                : "btn-success"} btn`}
            >
              {prediction.verdict ? "Set false" : "Set true"}
            </button>
          </p>
        ) : (
          <p>
            {"Verdict: "}
            {Date.parse(prediction.due_date) > Date.now()
              ? "due date not come "
              : "waiting for verdict "}
          </p>
        );

      return (
        <div className="prediction" key={i}>
          <Link to={`/users/${prediction.id}`}>{prediction.id}</Link>
          <p className="text">{prediction.description}</p>
          {verdict}
        </div>
      );
    });

    return (
      <div>
        <Header session={this.props.session} />
        <div className="container">
          <DebounceInput
            minLength={2}
            debounceTimeout={300}
            className="search"
            placeholder="Enter prediction description..."
            onChange={this.onSearch}
            value={this.state.search}
            type="text"
          />
          {predictions}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session.session,
    predictions: state.prediction.predictions
  };
}

export default connect(mapStateToProps, { loadPredictions, editPrediction })(
  PredictionListPage
);
