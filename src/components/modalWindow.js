import '../style/modalWindow.css';

import React from 'react';

class ModalWindow extends React.Component {
  render() {
    if (!this.props.show) {
      return null;
    }

    return (
      <div className="mask" onClick={() => this.props.onClick(false)}>
        <div className="modal">
          <p>{this.props.title}</p>
          <div className="controls">
            <button
              onClick={() => this.props.onClick(true)}
              className="btn btn-success"
            >
              Yes
            </button>
            <button
              onClick={() => this.props.onClick(false)}
              className="btn btn-danger"
            >
              No
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalWindow;
