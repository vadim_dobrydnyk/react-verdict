import '../style/header.css';

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { logOutUser } from '../store/actions/sessionActions';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
  }

  logOut() {
    this.props.logOutUser();
  }

  render() {
    return (
      <header className="header">
        <div className="container">
          <p className="project">Verdict</p>
          <span className="nav">
            <Link to="/users">Users</Link>
            <Link to="/predictions">Predictions</Link>
          </span>
          <p onClick={this.logOut} className="control">
            Logout
          </p>
        </div>
      </header>
    );
  }
}

export default connect(null, { logOutUser })(Header);
