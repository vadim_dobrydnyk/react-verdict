import './style/index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import LoginPage from './components/LoginPage';
import PredictionListPage from './components/PredictionListPage';
import RequireAuth from './components/RequireAuth';
import UsersListPage from './components/UsersListPage';
import UsersPage from './components/UsersPage';
import registerServiceWorker from './registerServiceWorker';
import { rootReducer } from './store/reducers/rootReducer';

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <Route exact path="/users" component={RequireAuth(UsersListPage)} />
        <Route exact path="/users/:id" component={RequireAuth(UsersPage)} />
        <Route
          exact
          path="/predictions"
          component={RequireAuth(PredictionListPage)}
        />
        <Redirect from="/" to="/users" />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
