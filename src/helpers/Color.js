class Color {
  static rand(min, max) {
    return parseInt(Math.random() * (max - min + 1), 10) + min;
  }

  static getRandomColor() {
    var h = this.rand(1, 360);
    var s = this.rand(39, 72);
    var l = this.rand(49, 59);
    return "hsl(" + h + "," + s + "%," + l + "%)";
  }
}

export default Color;
